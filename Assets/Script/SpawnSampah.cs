﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSampah : MonoBehaviour
{
    public float jeda = 0.8f;
    float timer;
    public GameObject[] obyekSampah;
    // Use this for initialization

    void Update()
    {
        // Start timer for spawning
        timer += Time.deltaTime;

        // When timer value is bigger than jeda
        if (timer > jeda)
        {
            // Set random trash type and make it intantiate
            int random = Random.Range(0, obyekSampah.Length);
            Instantiate(obyekSampah[random], transform.position, transform.rotation);
            timer = 0;
        }
    }
}
