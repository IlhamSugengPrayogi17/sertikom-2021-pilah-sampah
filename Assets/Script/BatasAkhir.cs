﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BatasAkhir : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collision)
    {
        // Destroy GameObject if This Object is Out of Bound
        Destroy(collision.gameObject);
    }
}
