﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    // True condition where gameplay started
    public bool Beginning = true;

    // Update is called once per frame
    void Update()
    {
        // If beginning is true and score is bigger than zero
        if (Beginning && Data.score > 0)
        {
            // Make beginning boolean to false
            Beginning = false;
        }

        // If beginning isn't true dan score is smaller or equal to zero
        if (!Beginning && Data.score <= 0)
        {
            // Move to GameOver scene
            SceneManager.LoadScene("GameOver");
        }
    }
}
