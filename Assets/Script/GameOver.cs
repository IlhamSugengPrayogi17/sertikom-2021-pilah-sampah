﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Timer variable
    float timer = 0;
    // Update is called once per frame
    void Update()
    {
        // Increment timer and start after game over
        timer += Time.deltaTime;

        // Game over screen-time before load scene Gameplay
        if (timer > 2)
        {
            // Reset Score to Zero
            Data.score = 0;

            // Move to GamePlay scene
            SceneManager.LoadScene("Gameplay");
        }
    }
}
